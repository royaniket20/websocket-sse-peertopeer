package com.aniket.websocketexperiment.config;

import com.aniket.websocketexperiment.controller.WebSocketHandlerJavaVariant;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

/**
 * This is Just based on JSR Variant
 */
@Configuration
@EnableWebSocket
public class WebSocketConfig  implements WebSocketConfigurer {


    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(webSocketHandlerJavaVariant(),"/echo-data"); // this Endpoint you need to use in you client code
        registry.addHandler(webSocketHandlerJavaVariant(),"/echo-data-sockjs").withSockJS(); //This is fallback way
    }

    @Bean
    public WebSocketHandlerJavaVariant webSocketHandlerJavaVariant(){
        return new WebSocketHandlerJavaVariant();
    }
}
