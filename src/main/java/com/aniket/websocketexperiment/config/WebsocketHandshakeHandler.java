package com.aniket.websocketexperiment.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;
import sun.security.acl.PrincipalImpl;

import java.security.Principal;
import java.util.Map;
import java.util.Random;
@Slf4j
public class WebsocketHandshakeHandler extends DefaultHandshakeHandler {

     private String[] ADJS = {"pale","rhetorical","noiseless","popular","clean"};
     private String[] NOUNS = {"river","laser","resign","day","spare"};

    @Override
    protected Principal determineUser(ServerHttpRequest request, WebSocketHandler wsHandler, Map<String, Object> attributes) {
        String userName = getRandom(NOUNS)+"_"+getRandom((ADJS));
        log.info("USER CONNECTED :::: {}",userName);
        return new PrincipalImpl(userName);
    }

    private String getRandom(String[] data){
        int random = new Random().nextInt(data.length-1);
        return data[random];

    }
}
