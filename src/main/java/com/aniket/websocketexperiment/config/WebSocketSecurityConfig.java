//package com.aniket.websocketexperiment.config;
//
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.web.messaging.MessageSecurityMetadataSourceRegistry;
//import org.springframework.security.config.annotation.web.socket.AbstractSecurityWebSocketMessageBrokerConfigurer;
//
///**
// *  JSR Websocket Implementation Do not have Security Config
// *  Generally when you use SockJS and STOMP  - Handshaking happens at the First AND THE oNLY hTTP connection during connection upgrade
// *  You shoud use some security mechanism
// *  You can protect Subscription
// *  You can protect the Message Publishing also
// */
//@Configuration
//public class WebSocketSecurityConfig extends AbstractSecurityWebSocketMessageBrokerConfigurer {
//    @Override
//    protected void configureInbound(MessageSecurityMetadataSourceRegistry messages) {
//        messages
//        //Restrict Subscription to a  Specific Topic
//                .simpSubscribeDestMatchers("/topic/admin-topic").hasRole("ADMIN")
//                //User casnnot send message to certain Destinations ....
//                .simpMessageDestMatchers("/topic/orders").denyAll() //Only application send Notification here
//                .anyMessage().authenticated();
//    }
//}
