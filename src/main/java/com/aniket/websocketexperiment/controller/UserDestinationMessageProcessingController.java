package com.aniket.websocketexperiment.controller;

import com.aniket.websocketexperiment.model.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.stereotype.Controller;

import java.security.Principal;

//You can use a Controller with Message Mapping - If you want to process a Message before getting Published to Subscribers
//Anything coming from  /app/<topic-name> will be procees bt Message habdler of that Topic name and then will be pushed to the Topic
@Controller
@Slf4j
public class UserDestinationMessageProcessingController {

    @Autowired
    private  SimpMessageSendingOperations messagingTemplate;
    @MessageMapping("/private-message/{targetUser}")
    public void processMessage(String message , Principal principal , @DestinationVariable String targetUser ) throws InterruptedException {
        log.info("Processing the message send to User Message Destination- Queue ----- Destination ---- user-destination --- {}",message);
        log.info("Message Coming From User --- {}",principal.getName());
        log.info("Message going to User --- {}",targetUser);
        Thread.sleep(1500);
        message = message.concat("PROCESSED USER MESSAGE !!! ");
        messagingTemplate.convertAndSendToUser(targetUser , "/queue/private-message" , message);
    }
}
