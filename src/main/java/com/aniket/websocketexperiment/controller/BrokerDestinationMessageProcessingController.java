package com.aniket.websocketexperiment.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import java.security.Principal;

//You can use a Controller with Message Mapping - If you want to process a Message before getting Published to Subscribers
//Anything coming from  /app/<topic-name> will be procees bt Message habdler of that Topic name and then will be pushed to the Topic
@Controller
@Slf4j
public class BrokerDestinationMessageProcessingController {

    @MessageMapping("/broker-destination-topic")
   // @SendTo("/topic/broker-destination-topic-second") //Using this you can choose at which Topic you need to redirect the traffic
    //By Default it will redirect to /topic/broker-destination-topic
    public String processMessage(String message , Principal principal) throws InterruptedException {
        log.info("Processing the message send to Message Broker - Topic ----- Destination ---- broker-destination-topic --- {}",message);
        log.info("Message Coming From User --- {}",principal.getName());
        Thread.sleep(1500);
        message = message.concat("PROCESSED IT !!! ");
        return message;
    }


}
