package com.aniket.websocketexperiment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.user.SimpUser;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ConnectedUserController {

    @Autowired
    private SimpUserRegistry userRegistry;


    @GetMapping(path = "/connected-users")
    public List<String> listUsers() {
        List<String> result = new ArrayList<>();
        for (SimpUser user : this.userRegistry.getUsers()) {
            result.add(user.toString());
        }
        return result;
    }
}
