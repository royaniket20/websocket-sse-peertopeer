package com.aniket.websocketexperiment.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.socket.*;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * This Handler will Handle all the messages Coming to it
 * Because Websocket can Publish Both Binary and Text Data - Spring Implemented Two casses - Text /Binary - We using Text One
 */

/**
 * We shoud use STOMP protocal - Higher Level API
 * Similar to JMS
 * Broker --- understand the STOMP Protocol
 * Broker Destination --- When you send a message to a path - Matching with Broker - it will directly go to Message Broker [Broadcasing to all connected listenr]
 * Application Destination  ---- Message will delegate to a Message Controller [Message Handler Method]
 * User Destination ----- Message will be routed to Specific User [Peer to Peer ]
 * There are types of Destinations ----
 *
 *
 * Broker can be of two types ----
 *
 * Simple Broker ---
 * Built-in-broker
 * everything in Memory
 *
 * Broker Relay ---
 * Forward message to STOMP Broker [Active MQ , Rabbit MQ ]
 * Better for scaling
 */
@Slf4j
public class WebSocketHandlerJavaVariant extends TextWebSocketHandler {
    private final List<WebSocketSession> webSocketSessions = new ArrayList<>();


    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        log.info("----- After Connection is Established ------ {}", session);
        webSocketSessions.add(session);
    }

    @Override
    protected void handlePongMessage(WebSocketSession session, PongMessage message) throws Exception {
        log.info("----- On handling Pong Message ------ {} ===> {}", session, message);
        for (WebSocketSession webSocketSession : webSocketSessions
        ) {
            webSocketSession.sendMessage(message);
        }
    }

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        log.info("----- On handling Actual Txt  Message ------ {} ===> {}", session, message);
        for (WebSocketSession webSocketSession : webSocketSessions) {
            webSocketSession.sendMessage(message);
        }
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        log.info("----- On handling Transport Error ------ {} ===> {}", session, exception.getMessage());
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) throws Exception {
        log.info("----- On connection closure ------ {} ===> {}", session, closeStatus);
        webSocketSessions.remove(session);
    }

    @Override
    public boolean supportsPartialMessages() {
        log.info("Is supporting partial Message -- false");
        return false;
    }
}
