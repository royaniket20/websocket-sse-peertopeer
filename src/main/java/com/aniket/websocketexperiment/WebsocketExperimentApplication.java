package com.aniket.websocketexperiment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebsocketExperimentApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebsocketExperimentApplication.class, args);
	}
/**
 * Some reference Url ---
 * https://www.baeldung.com/spring-websockets-send-message-to-user
 * https://www.youtube.com/watch?v=nxakp15CACY
 * https://mokkapps.de/blog/sending-message-to-specific-anonymous-user-on-spring-websocket/ --- using /user endpoint you can even Listen to Only your Messages via Topic also
 *  -- /user/topic/greetings instead of /topic/greetings
 */
}
