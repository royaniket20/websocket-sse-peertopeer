package com.aniket.websocketexperiment.constants;

public enum SSEEventEnum {
    STOCK,
    FUTURE,
    OPTION;
}
